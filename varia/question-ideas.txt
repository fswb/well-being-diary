Kindness?
"What acts of kindness did I see today?(Could be towards me or between other people, or in a film or tv series)"

Contributing and supporting others
"What's one thing I did to support or contribute to the well-being of another person today? It might be something material or practical, or listening to someone, or setting a positive intention which will effect how I interact with this person in the future."

Connection to other people and to the world
"What are some things that i did that are important to other people? It could be a thing as simple as staying aware of my breathing, being peaceful, etc. Did I remember that I am practicing for everyone and not just myself? Did I see that my way of being will model a way for others also?"

Activities of friends and family

Metta phrases

Gratitude
"What's one thing I am grateful for today? It may be something ordinary, something i see every day, or it may be something unusual. It could be a small thing or a big thing. It can be freedom from suffering.
What feelings did i feel and what needs were met? What were the details of the experience?"

Mindfulness
"Was I able to stay mindful of my steps when walking outside and inside? How many steps did I take for each breath? Could i maintain my mindfulness of breathing for long periods of time? In one of my daily activities, did I (in the moment) remember my connection to other people and to the world?"

Learning something helpful
"What's one thing that I learned today that I can take with me and that could help me increase happiness and peace, and reduce suffering?"

