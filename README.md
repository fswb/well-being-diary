Project status: Pre-alpha

# Well-being Diary

Chat: https://matrix.to/#/!lvzqRrHTyJlAVhyGPP:fairydust.space?via=fairydust.space

A happiness and well-being diary application. It's a cross-platform desktop application and is in an early development/prototype stage

**Table of contents:**

1. [Description](#description)
2. [Installation](#installation)
3. [Usage](#usage)
4. [Feedback](#feedback)


## Description

The user can write questions for herself that can be used to support happiness, compassion and well-being

### Screenshots



### License

GPLv3


## Installation

pip3 install well-being-diary

### Advanced setup (optional)

Please note: *This is not necessary for running the application*, instead you can skip directly to the [usage](#usage) section

#### GNU/Linux systems

For desktop systems that are compatible with the [freedesktop](https://www.freedesktop.org/) standard - for example Gnome and KDE - you can use the bwb.desktop file included in the source (please note that if using a file manager such as the Gnome file manager you may see the name displayed as "Well-being Diary" rather than the file name) to make the application visible in any start-menu-like menu (in Lubuntu this is called the "main menu" and it's shown when clicking the button in the lower left, "vanilla" (the ordinary) Ubuntu may not have a menu like this

To use this file:

1. Edit the `well-being-diary.desktop` file and change the paths to match the path that you are using
2. Copy the `well-being-diary.desktop` file to your desktop or any place where you want to be able to start the application from
3. Copy the `well-being-diary.desktop` file to `/usr/share/applications/` using `sudo`

### Hardware recommendations

* Works best on screens with a resolution of at least 1366x768
* No network connection is needed
* Does not take much processor and memory resources, expected to run smoothly on most system 


## Usage

1. Change directory to where the software files have been extracted
2. Type and run `python3 well-being-diary.py` on GNU/Linux systems or `python well-being-diary.py` on Windows

### Testing

Alternatively you can start the application with the `--testing` flag, this will make sure that the application data is stored in memory only and not saved when the application is closed

## Feedback

Feedback is very welcome! If you send us feedback it can help improve the software

### Ideas for improving the software

https://gitter.im/well-being-diary/community

### Reporting bugs

Please use the GitHub issue tracker: https://github.com/SunyataZero/well-being-diary/issues

Don't hesitate to file a bug! *You are helping to improve the software*. Also if something is unclear in the *documentation* that counts as a bug as well, so please report it

### What is already working well

This is good to know so that we know what to keep, also it gives motiviation to continue working on the software <3
