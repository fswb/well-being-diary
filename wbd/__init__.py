import logging
logging.basicConfig(level=logging.NOTSET)
# -These lines are needed to make the logger available for each file in the project
#  This may be a quick and dirty solution since i haven't found it anywhere else!
#  Other solutions here: https://stackoverflow.com/questions/15727420/using-python-logging-in-multiple-modules
# -Documentation: https://docs.python.org/3.7/library/logging.html#logging.basicConfig
